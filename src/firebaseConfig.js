import firebase from "firebase";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyDVnnsGPW6mE_WmmzwjqSsWB2m69mBFrr0",
    authDomain: "worklogly.firebaseapp.com",
    databaseURL: "https://worklogly.firebaseio.com",
    projectId: "worklogly",
    storageBucket: "worklogly.appspot.com",
    messagingSenderId: "306564737033",
    appId: "1:306564737033:web:df5ff8224592a5f568f0d8",
    measurementId: "G-8Y3MP4KRSD"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

export default db;