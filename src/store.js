import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import db from './firebaseConfig.js';

export default new Vuex.Store({
    state: {
        todos: [
        ],
    },

    mutations: {
        setTodos(state, todos) {
            state.todos = todos;
        },

        addTodo(state, todo) {
            state.todos.unshift(todo);
        },
        
        deleteTodo(state, id) {
            state.todos.splice(state.todos.findIndex(todo => todo.id == id), 1);
        }
    },

    getters: {
        completedTodos: state => {
            return state.todos.filter(todo => todo.completed).length;
        }
    },

    actions: {
        fetchTodos(context) {
            db.collection('todos')
            .get()
            .then((querySnapshot) => {
                let todos = [];
                querySnapshot.forEach(doc => {
                    todos.push(doc.data());
                })
                context.commit('setTodos', todos);
            })
            .catch((error) => {
                console.log("error occurred in loading todos...", error);
            })
        },

        addTodo(context, todo) {
            return new Promise((resolve, reject) => {
                let todoObj = Object.assign({}, todo);
                db.collection('todos').doc(todo.id).set(todoObj)
                .then(() => {
                    context.commit('addTodo', todoObj);
                    resolve();
                    console.log("saved todo");
                })
                .catch((error) => {
                    reject(error);
                    console.log("error while saving: ", error);
                })
            });
        },

        deleteTodo(context, id) {
            return new Promise((resolve, reject) => {
                db.collection('todos').doc(id).delete()
                .then(() => {
                    context.commit('deleteTodo', id);
                    resolve();
                    console.log("deleted todo with id: ", id);
                })
                .catch((error) => {
                    reject(error);
                    console.log("error occurred during deletion: ", error);
                })
            })
        }
    }
})