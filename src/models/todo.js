class TodoModel {
    constructor() {
        this.id = String((new Date()).getTime());
        this.title = '';
        this.completed = false;
    }
}

export default TodoModel;